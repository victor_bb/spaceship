﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    public GameObject asteroid;
    public GameObject asteroid2;
    public GameObject asteroid3;
    public GameObject enemyShip;

    public float minDelay;
    public float maxDelay;
    public float asteroidIncrease;

    public float minSpeed;
    public float maxSpeed;

    private float nextSpawn;//время следующего астеройда
    private float asteroidCount = 1;

    private List<GameObject> asteroids = new List<GameObject>();

    private void Start()
    {
        asteroids.Add(asteroid);
        asteroids.Add(asteroid);
        asteroids.Add(enemyShip);
        asteroids.Add(asteroid2);
        asteroids.Add(asteroid2);
        asteroids.Add(enemyShip);
        asteroids.Add(asteroid2);
        asteroids.Add(asteroid3);
        asteroids.Add(enemyShip);
        asteroids.Add(asteroid3);
        asteroids.Add(asteroid3);
    }

    void Update()
    {
        if (Time.time > nextSpawn)
        {
            float maxAsteroid = Random.Range(1, asteroidCount);

            for (int i = 0; i < maxAsteroid; i++) {
                float randomXPosition = Random.Range(-transform.localScale.x / 2, transform.localScale.x / 2);
                Vector3 randomPosition = new Vector3(
                    randomXPosition,
                    transform.position.y,
                    transform.position.z
                );

                int index = Random.Range(0, 11);
                //Debug.Log(index);
                GameObject someEnemy = (GameObject)Instantiate(asteroids[index], randomPosition, Quaternion.identity);

                if ("EnemyShip" == someEnemy.tag) {
                    Rigidbody enemyShipRigid = someEnemy.GetComponent<Rigidbody>();
                    enemyShipRigid.rotation = Quaternion.Euler(0, 180, 0);
                    enemyShipRigid.velocity = Vector3.back * Random.Range(minSpeed, maxSpeed);
                }
                
            }

            asteroidCount += asteroidIncrease;
            nextSpawn += Random.Range(minDelay, maxDelay);
        }
    }
}
