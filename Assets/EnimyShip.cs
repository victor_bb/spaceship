﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnimyShip : MonoBehaviour
{

    public Transform leftLazerGun;
    public Transform rightLazerGun;
    public GameObject lazerShotShort;
    public GameObject explosion;
    public GameObject enemyExplosion;
    public float shotDelay = 3;
    public float minSpeed;
    public float maxSpeed;
    public float lazerShotShortSpeed;


    private float nextShot;
    private Rigidbody ship;

    private Vector3 leftBulletVelocity;
    private Quaternion leftBulletRotation;
    private Vector3 rightBulletVelocity;
    private Quaternion rightBulletRotation;

    // Start is called before the first frame update
    void Start()
    {
        ship = GetComponent<Rigidbody>();

        // не позволит менять направление в другом скрипте
        // видимо не пересетить или старт вызывается в момент появления на экране
        //ship.rotation = Quaternion.Euler(0, 180, 0);
        //ship.velocity = Vector3.back * Random.Range(minSpeed, maxSpeed);

        leftBulletVelocity = new Vector3(0, 0, -1) * lazerShotShortSpeed;
        rightBulletVelocity = new Vector3(0, 0, -1) * lazerShotShortSpeed;
        leftBulletRotation = Quaternion.Euler(0, 0, 0);
        rightBulletRotation = Quaternion.Euler(0, 0, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GameArea" || other.tag == "EnemyLazerShot")
        {
            return;
        }

        Instantiate(enemyExplosion, transform.position, transform.rotation);

        Destroy(gameObject);
        Destroy(other.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        bool canShot = Time.time > nextShot;

        if (canShot)
        {
            nextShot = Time.time + shotDelay;
            
            GameObject leftLazerShot = (GameObject)Instantiate(lazerShotShort, leftLazerGun.position, Quaternion.identity);
            Rigidbody leftBullet = leftLazerShot.GetComponent<Rigidbody>();
            leftBullet.velocity = leftBulletVelocity;
            leftBullet.rotation = leftBulletRotation;

            GameObject rightLazerShot = (GameObject)Instantiate(lazerShotShort, rightLazerGun.position, Quaternion.identity);
            Rigidbody rightBullet = rightLazerShot.GetComponent<Rigidbody>();
            rightBullet.velocity = rightBulletVelocity;
            rightBullet.rotation = rightBulletRotation;
        }
    }
}
