﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipController : MonoBehaviour
{
    public float minDelay;
    public float maxDelay;
    public float enemyShipIncrease;
    public GameObject enemyShip;

    private float nextSpawn;//время следующего астеройда
    private float shipCount = 1;
    private GameObject playerShip;
    // Start is called before the first frame update
    void Start()
    {
        if (playerShip == null)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

            playerShip = players[0];
        }
    }

    void Update()
    {
        if (Time.time > nextSpawn)
        {
            float randomZPosition = Random.Range(-transform.localScale.z / 2, transform.localScale.z / 2);
            Vector3 randomPosition = new Vector3(
                transform.position.x,
                transform.position.y,
                randomZPosition
            );

            GameObject enemyShipInstance = (GameObject)Instantiate(enemyShip, randomPosition, Quaternion.identity);
            Rigidbody enemyShipRigid = enemyShipInstance.GetComponent<Rigidbody>();

            if (null != playerShip)
            {
                Vector3 c = playerShip.transform.position;

                Vector3 vectorToPlayerShip = enemyShipInstance.transform.position - playerShip.transform.position;

                Vector3 currentOffset = enemyShipInstance.transform.InverseTransformPoint(enemyShipInstance.transform.position);
                Vector3 desiredOffset = playerShip.transform.InverseTransformPoint(playerShip.transform.position);
                enemyShipInstance.transform.rotation = Quaternion.FromToRotation(currentOffset, desiredOffset);

                Debug.Log(vectorToPlayerShip.x);
                Debug.Log(vectorToPlayerShip.y);
                Debug.Log(vectorToPlayerShip.z);


                enemyShipRigid.velocity = new Vector3(-vectorToPlayerShip.x, 0, -vectorToPlayerShip.z);
                enemyShipRigid.rotation = Quaternion.Euler(-vectorToPlayerShip.x, 0, -vectorToPlayerShip.z);
            } else {
                enemyShipRigid.rotation = Quaternion.Euler(0, 180, 0);
                enemyShipRigid.velocity = Vector3.back * Random.Range(2, 4);
            }

            shipCount += enemyShipIncrease;
            nextSpawn += Random.Range(minDelay, maxDelay);
        }
    }

    private Vector3 generateMovement(GameObject enemyShipInstance)
    {
        if (null != playerShip) {
            Vector3 c = playerShip.transform.position;
            
            return enemyShipInstance.transform.position - playerShip.transform.position;
        }

        return new Vector3(0, 0, 0);
    }
}
