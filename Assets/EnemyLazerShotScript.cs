﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLazerShotScript : MonoBehaviour
{
    public GameObject playerExplosion;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, transform.position, transform.rotation);
            Destroy(gameObject);
            Destroy(other.gameObject);
        }
    }
}
