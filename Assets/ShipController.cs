﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public float tilt;
    public float speed;
    public float xMin;
    public float xMax;
    public float zMin;
    public float zMax;
    public float lazerShotSpeed;
    public float lazerShotShortSpeed;

    public GameObject lazerShot;
    public GameObject lazerShotShort;
    public Transform lazerGun;
    public Transform leftLazerGun;
    public Transform rightLazerGun;
    public float shotDelay;
    public float shortShotDelay;

    private Rigidbody ship;
    private float nextShot;
    private float nextSideShot;
    private Vector3 leftBulletVelocity;
    private Quaternion leftBulletRotation;
    private Vector3 rightBulletVelocity;
    private Quaternion rightBulletRotation;

    // Start is called before the first frame update
    void Start()
    {
        ship = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        ship.velocity = new Vector3(moveHorizontal, 0, moveVertical) * speed;

        float xPosition = Mathf.Clamp(ship.position.x, xMin, xMax);
        float zPosition = Mathf.Clamp(ship.position.z, zMin, zMax);

        ship.position = new Vector3(xPosition, 0, zPosition);
        ship.rotation = Quaternion.Euler(ship.velocity.z * tilt, 0, - ship.velocity.x * tilt);

        leftBulletVelocity = new Vector3(-1, 0, 1) *lazerShotShortSpeed;
        rightBulletVelocity = new Vector3(1, 0, 1) *lazerShotShortSpeed;
        leftBulletRotation = Quaternion.Euler(0, -45, 0);
        rightBulletRotation = Quaternion.Euler(0, 45, 0);
    }

    private void Update()
    {
        bool canShot = Time.time > nextShot;

        if (Input.GetButton("Fire1") && canShot) {
            nextShot = Time.time + shotDelay;
            GameObject copyLazerShot = (GameObject)Instantiate(lazerShot, lazerGun.position, Quaternion.identity);

            //GameObject copyLazerShot = (GameObject) Instantiate(lazerShot, lazerGun.position, Quaternion.identity);
            
            Rigidbody bullet = copyLazerShot.GetComponent<Rigidbody>();//.velocity = new Vector3(0, 180f, 0);
            bullet.velocity = Vector3.forward * lazerShotSpeed;

            Destroy(copyLazerShot, 1.5f);
        }

        bool canSideShot = Time.time > nextSideShot;

        if (Input.GetButton("Fire1") && canSideShot)
        {
            nextSideShot = Time.time + shortShotDelay;

            GameObject leftLazerShot = (GameObject)Instantiate(lazerShotShort, leftLazerGun.position, Quaternion.identity);
            Rigidbody leftBullet = leftLazerShot.GetComponent<Rigidbody>();
            leftBullet.velocity = leftBulletVelocity;
            leftBullet.rotation = leftBulletRotation;

            GameObject rightLazerShot = (GameObject)Instantiate(lazerShotShort, rightLazerGun.position, Quaternion.identity);
            Rigidbody rightBullet = rightLazerShot.GetComponent<Rigidbody>();
            rightBullet.velocity = rightBulletVelocity;
            rightBullet.rotation = rightBulletRotation;
        }
    }
}
